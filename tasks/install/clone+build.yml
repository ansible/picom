---

- name: Clone and rebuild picom
  when: >
    not (picom_repo_check.stat.exists | bool) or
    (picom.update | bool)
  become: true
  become_user: "{{ ansible_env.USER }}"
  block:

    # the picom readme suggests using `git submodule update --init --recursive`, but 
    # recent versions of git should check out submodules automatically, I think
    - name: "Clone picom {{ picom_release_version.stdout }}"
      ansible.builtin.git:
        repo: "https://github.com/yshui/picom.git"
        dest: "{{ picom.repo }}"
        accept_hostkey: yes
        version: "{{ picom_release_version.stdout }}"
        force: yes

    - name: "Setup Python virtual environment with meson and ninja"
      ansible.builtin.import_tasks: install/pyvenv.yml

    # use the meson build system (written in python), to make a ninja build
    # For picom v9.1 I get a build error that appears related to GL:
    # > Run-time dependency gl found: NO (tried system)
    # > src/meson.build:62:1: ERROR: Could not generate cargs for gl:
    # > Package 'xfixes' requires 'fixesproto >= 6.0' but version of fixesproto is 5.0
    # since libGL is optional, lets disable it with the -Dopengl=false flag
    # I suspect this is a problem due to Ubuntu 18.04 being old)
    # There is also a serious-looking warning about soon-to-deprecated cmake version
    - name: Use the meson build system to make a ninja build
      ansible.builtin.shell: >
        source {{ picom_venv_bin }}/activate && meson
        --buildtype=release
        -Dopengl=false
        . build
      args:
        chdir: "{{ picom.repo }}"
        executable: /bin/bash
  # END OF BLOCK


# Use the ninja build file to install picom
# default install prefix is /usr/local, that is,
# the built picom binary is copied from build/src/ to /usr/local/bin/
# This task must be run as root
- name: "ninja -C build install"
  ansible.builtin.shell: >
    source {{ picom_venv_bin }}/activate &&
    ninja -C build install
  args:
    chdir: "{{ picom.repo }}"
    executable: /bin/bash
